require 'haml'

module Jekyll
  module Haml
    # parse module
    class Parser
      #CONFIG = { attr_wrapper: '"', escape_attrs: false }.freeze

      def self.matches(ext)
        ext =~ /^\.haml$/i
      end

      def self.compile(content)
        #::Haml::Template.options[:attr_wrapper] = '"'
        ::Haml::Template.options[:escape_attrs] = false
        #template = ::Haml::Engine.new content, CONFIG
        template = ::Haml::Template.new { content }
        #.render.split("\n").join
        template
      end
    end
  end
end
